# PolyVent Control Module (Electronics)

This control module includes the electronics (and the maehcanical frame 
needed to hold them.)

This was designed by Nathaniel Bechard. It is a "backplane" of pluggable 
cards using the "European half-card" standard. The backplane is connected
to the pins of the ESP32.

This design allows the extention of functionality by creating new cards.
The cards at present are:
1. The main processor card
1. The valve card 
1. The pressure sensor card.

Additionally, the [General Purpose Alarm Device (GPAD)](https://github.com/PubInv/general-purpose-alarm-device) project designed an extention to interface to the GPAD to provide alarms.

# DOI

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.10642659.svg)](https://doi.org/10.5281/zenodo.10642659)
