![IMG_5138](uploads/5fb007667dbb8a8b6e8d8c31e5037534/IMG_5138.JPG)
### 3.4.1 Purpose of the card
To control solenoid valves, proportional valves and any other 12V-24V device (as long as it draws less than X A) 
### 3.4.2 Card features
 - 12 Valve Channels 
 - Optical isolation with an EL817(D)-ND
 - PWM control
 - 48MHz SAMD21 microcontroller

### 3.4.3 Electrical specifications
follows all polyvent control module standards

Maximum current draws on for each port:  
| device | maximum current draws |
| ------ | ------ |
| individual port | XA |
| all ports combined (12 ports) | 12A |

Default SPI CS pin:
CS_4


### 3.4.4 Front plate mechanical specifications
in the CAD, will be added to the repo shortly
