## Documentation for the V2.1 PolyVent control module.

- [1.0: Control module overview](1.0-system-overview)  
- 2.0: Overall system specifications and standards  
    - [2.1: Electrical specification and standards](2.1-Electrical-Specifications)    
    - [2.2: Mechanical specifications and standards](2.2-Mechanical-Specifications)    
- 3.0: Oficial modules - tested and supported  
    - [3.1: PolyVent stepper motor control card](3.1-PolyVent-Stepper-Motor-Control-Card)  
    - [3.2: PolyVent sensing card](3.2-PolyVent-Sensing-Card)  
    - [3.3: PolyVent ESP32 card](3.3-PolyVent-Microcontroller-ESP32-Card)   
    - [3.4: PolyVent solenoid valve control card](3.4-PolyVent-Valve-Card)  
    - [3.5: PolyVent prototyping card](3.5-PolyVent-Prototype-Card)   
- 4.0: Oficial module configurations
    - [4.1: Double bellows configuration](4.1-Double-Bellows-Configuration)
    - [4.2: Proportional valve ventilator configuration](4.2-Proportional-Valve-Ventilator-Configuration)
- 5.0: Build the design yourself
    - [5.1: Boards](5.1-Board-Assembly-Notes)